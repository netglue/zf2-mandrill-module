<?php

return array(
	
	'factories' => array(
		'NetglueMandrill\Client\MandrillClient' => 'NetglueMandrillModule\Factory\MandrillClientFactory',
		'NetglueMandrillModule\Factory\MandrillMessageFactory' => 'NetglueMandrillModule\Factory\MandrillMessageFactory',
	),
	
);
