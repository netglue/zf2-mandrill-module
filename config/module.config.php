<?php

return array(
	'netglue_mandrill' => array(

		/**
		 * Provide an API Key in one of your .local.php config files
		 */
		'api_key' => NULL,

		/**
		 * This provides message defaults to the Message Factory
		 * @see NetglueMandrillModule\Factory\MandrillMessageFactory
		 *
		 * The idea is to provide as much default config as you can so that
		 * there's minimal work to do when you want to send a message of a specific type
		 * Each array key is how you create your pre-configured message:
		 * $message = $factory->createMessage('messageName');
		 */
		'message_spec' => array(
			/*


			'messageName' => array(
				'template_name' => 'my-template-name',
				'message' => array(
					'to' => array(
						array(
							'name' => 'Some One',
							'email' => 'me@example.com',
							'type' => 'to',
						),
					),
					'headers' => array(
						'X-Foo-Bar' => 'Ding Dong',
					),
					'track_opens' => true,
					'merge' => true,
					'global_merge_vars' => array(
						array(
							'name' => 'MERGE1',
							'content' => 'Merge 1 Content',
						),
					),
					'tags' => array(
						'Some Tag', 'Some Other Tag',
					),
					'google_analytics_domains' => array(
						'adomain.com',
						'otherdomain.com',
					),
				),
			),

			*/
		),
	),
);
