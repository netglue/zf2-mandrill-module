<?php

return array(
	'netglue_mandrill' => array(
		// Provide a test key so you don't get any mail
		'api_key' => 'TestApiKey',
		
		'testing' => array(
			// Fill out with valid address
			'to' => '',
			'from' => '',
		),
	),
);
