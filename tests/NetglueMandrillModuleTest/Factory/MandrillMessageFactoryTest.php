<?php

namespace NetglueMandrillModule\Factory;
use PHPUnit_Framework_TestCase as TestCase;
use NetglueMandrillModuleTest\bootstrap;

class MandrillMessageFactoryTest extends TestCase
{

    public function testNewInstance()
    {
        $factory = new MandrillMessageFactory;
        return $factory;
    }

    /**
     * @depends testNewInstance
     */
    public function testSetGetClient(MandrillMessageFactory $factory)
    {
        $this->assertNull($factory->getMandrillClient());
        $sl = bootstrap::getServiceManager();
        $client = $sl->get('NetglueMandrill\Client\MandrillClient');
        $this->assertSame($factory, $factory->setMandrillClient($client));
        $this->assertSame($client, $factory->getMandrillClient());

        return $factory;
    }

    /**
     * @depends testSetGetClient
     */
    public function testSetGetMessageSpecs(MandrillMessageFactory $factory)
    {
        $this->assertEmpty($factory->getMessageSpecs());
        $specs = array(
            'foo' => array(
                'blah' => 'bar',
            ),
            'bar' => array(
                'boo' => 'baz',
            ),
        );
        $factory->setMessageSpecs($specs);
        $this->assertSame($specs, $factory->getMessageSpecs());

        return $factory;
    }

    /**
     * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
     * @depends testSetGetMessageSpecs
     */
    public function testSetMessageSpecsThrowsExceptionForNonArray(MandrillMessageFactory $factory)
    {
        $factory->setMessageSpecs('foo');
    }

    /**
     * @depends testSetGetMessageSpecs
     */
    public function testAddMessageSpec(MandrillMessageFactory $factory)
    {
        $factory->addMessageSpec('baz', array(
            'bing' => 'bong',
        ));
        $this->assertCount(3, $factory->getMessageSpecs());
        $this->assertArrayHasKey('baz', $factory->getMessageSpecs());

        return $factory;
    }

    /**
     * @depends testAddMessageSpec
     * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
     */
    public function testAddMessageSpecThrowsExceptionForNonArray(MandrillMessageFactory $factory)
    {
        $factory->addMessageSpec('baz', 'bat');
    }

    /**
     * @depends testAddMessageSpec
     */
    public function testCreateMessage(MandrillMessageFactory $factory)
    {
        $msg = $factory->createMessage();
        $this->assertInstanceOf('NetglueMandrillModule\Message\MandrillMessage', $msg);
        $this->assertSame($factory->getMandrillClient(), $msg->getMandrillClient());

        $spec = array(
            'template_name' => 'some-template-slug',
            'message' => array(
                'subject' => 'SUBJECT',
                'inline_css' => true,
                'tags' => array(
                    'Tag 1',
                    'Tag 2',
                ),
            ),
        );

        $factory->addMessageSpec('myMsg', $spec);
        $this->assertSame($spec, $factory->getMessageSpecByName('myMsg'));

        $msg = $factory->createMessage('myMsg');
        $this->assertSame('some-template-slug', $msg->getTemplateSlug());
        $this->assertSame('SUBJECT', $msg->getSubject());
        $this->assertCount(2, $msg->getTags());
    }

    /**
     * @depends testAddMessageSpec
     */
    public function testGetMessageSpecByNameReturnsNull(MandrillMessageFactory $factory)
    {
        $this->assertNull($factory->getMessageSpecByName('unknown'));
    }

    /**
     * @depends testAddMessageSpec
     * @expectedException NetglueMandrillModule\Exception\RuntimeException
     */
    public function testCreateMessageThrowsExceptionForUnknownSpec(MandrillMessageFactory $factory)
    {
        $factory->createMessage('unknown');
    }

}
