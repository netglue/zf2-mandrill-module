<?php

namespace NetglueMandrillModuleTest\Message;

use PHPUnit_Framework_TestCase as TestCase;

use NetglueMandrillModuleTest\bootstrap;

use NetglueMandrillModule\Message\MandrillMessage;
use NetglueMandrill\Client\MandrillClient;
use NetglueMandrillModule\Validator\MandrillTag as TagValidator;

/**
 * @coversDefaultClass NetglueMandrillModule\Message\MandrillMessage
 */
class MandrillMessageTest extends TestCase {
	
	public function testInstantiateMessage() {
		$msg = new MandrillMessage;
		$this->assertInstanceOf('NetglueMandrillModule\Message\MandrillMessage', $msg);
	}
	
	/**
	 * @covers ::setMandrillClient
	 * @covers ::getMandrillClient
	 */
	public function testSetGetMandrillClient() {
		$msg = new MandrillMessage;
		
		$client = new MandrillClient('foo');
		
		$this->assertNull($msg->getMandrillClient());
		$this->assertSame($msg, $msg->setMandrillClient($client), 'setMandrillClient() should be fluid');
		$this->assertSame($client, $msg->getMandrillClient(), 'getMandrillClient() shuld return the same instance set');
	}
	
	/**
	 * @covers ::setMessageConfig
	 * @covers ::getMessageConfig
	 */
	public function testSetGetMessageConfig() {
		$msg = new MandrillMessage;
		$config = array(
			'foo' => 'bar',
		);
		$this->assertSame($msg, $msg->setMessageConfig($config), 'setMessageConfig() should be fluid');
		$this->assertSame($config, $msg->getMessageConfig(), 'getMessageConfig() should return the same unmodified array');
	}
	
	/**
	 * @covers ::setSubject
	 * @covers ::getSubject
	 */
	public function testSetGetSubject() {
		$msg = new MandrillMessage;
		$expect = array(
			'subject' => 'Test',
		);
		$this->assertNull($msg->getSubject(), 'Subject should be initially null');
		$this->assertSame($msg, $msg->setSubject('Test'), 'setSubject() should be fluid');
		$this->assertSame('Test', $msg->getSubject(), 'Did not receive the subject set');
		$this->assertSame($expect, $msg->getMessageConfig(), 'Unexpected message config array');
	}
	
	/**
	 * @covers ::setAutoText
	 * @covers ::getAutoText
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 * @covers ::isAutoTextExplicitySet
	 */
	public function testSetGetAutoText() {
		$msg = new MandrillMessage;
		$this->assertFalse($msg->isAutoTextExplicitySet());
		$this->assertNull($msg->getAutoText());
		$this->assertSame($msg, $msg->setAutoText(true));
		$this->assertTrue($msg->isAutoTextExplicitySet());
		$this->assertTrue($msg->getAutoText());
		$this->assertSame($msg, $msg->setAutoText(false));
		$this->assertTrue($msg->isAutoTextExplicitySet());
		$this->assertFalse($msg->getAutoText());
		$this->assertSame($msg, $msg->setAutoText(NULL));
		$this->assertFalse($msg->isAutoTextExplicitySet());
		$this->assertNull($msg->getAutoText());
	}
	
	/**
	 * @covers ::setBoolParam
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Unknown property name test
	 */
	public function tesSetBoolParamThrowsExceptionForInvalidParam() {
		$msg = new MandrillMessage;
		$msg->setBoolParam('test', true);
	}
	
	/**
	 * @covers ::getBoolParam
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Unknown property name test
	 */
	public function tesGetBoolParamThrowsExceptionForInvalidParam() {
		$msg = new MandrillMessage;
		$msg->getBoolParam('test');
	}
	
	/**
	 * @covers ::setAutoHtml
	 * @covers ::getAutoHtml
	 * @covers ::isAutoHtmlExplicitySet
	 */
	public function testSetGetAutoHtml() {
		$msg = new MandrillMessage;
		$this->assertFalse($msg->isAutoHtmlExplicitySet());
		$this->assertNull($msg->getAutoHtml());
		$this->assertSame($msg, $msg->setAutoHtml(true));
		$this->assertTrue($msg->isAutoHtmlExplicitySet());
		$this->assertTrue($msg->getAutoHtml());
		$this->assertSame($msg, $msg->setAutoHtml(false));
		$this->assertTrue($msg->isAutoHtmlExplicitySet());
		$this->assertFalse($msg->getAutoHtml());
	}
	
	/**
	 * @covers ::addRecipient
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage not a valid recipient type
	 */
	public function testAddRecipientThrowsExceptionForInvalidType() {
		$msg = new MandrillMessage;
		$msg->addRecipient('test', 'test', 'test');
	}
	
	/**
	 * @covers ::addRecipient
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Email address should be a string
	 */
	public function testAddRecipientThrowsExceptionForInvalidEmail() {
		$msg = new MandrillMessage;
		$msg->addRecipient(array(), 'test', 'to');
	}
	
	/**
	 * @covers ::addRecipient
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Email address must be a non-empty string
	 */
	public function testAddRecipientThrowsExceptionForEmptyEmail() {
		$msg = new MandrillMessage;
		$msg->addRecipient('', 'test', 'to');
	}
	
	/**
	 * @covers ::addRecipient
	 * @covers ::getRecipients
	 * @covers ::clearRecipients
	 */
	public function testAddRecipient() {
		$msg = new MandrillMessage;
		$this->assertInternalType('array', $msg->getRecipients(), 'getRecipients should always return an array');
		$this->assertEmpty($msg->getRecipients(), 'Recipients should be initially empty');
		$returned = $msg->addRecipient(' Test@Example.COM ', 'Test', ' CC ');
		$this->assertSame($msg, $returned, 'addRecipient() should be fluid');
		$all = $msg->getRecipients();
		$this->assertCount(1, $all);
		$spec = current($all);
		$this->assertArrayHasKey('email', $spec);
		$this->assertArrayHasKey('name', $spec);
		$this->assertArrayHasKey('type', $spec);
		$this->assertSame('test@example.com', $spec['email'], 'Email address should be trimmed and lowered');
		$this->assertSame('Test', $spec['name'], 'Unexpected recipient name');
		$this->assertSame('cc', $spec['type'], 'Recipient type should be trimmed and lowered');
		
		$msg->addRecipient('test@example.com', 'Foo', 'to');
		$all = $msg->getRecipients();
		$this->assertCount(1, $all, 'Using the same email address should not increase the recipient count');
		$spec = current($all);
		$this->assertSame('test@example.com', $spec['email']);
		$this->assertSame('Foo', $spec['name'], 'Unexpected recipient name');
		$this->assertSame('to', $spec['type']);
		
		$this->assertSame($msg, $msg->clearRecipients(), 'clearRecipients() should be fluid');
		$this->assertCount(0, $msg->getRecipients());
	}
	
	/**
	 * @covers ::removeRecipient
	 * @covers ::addRecipient
	 * @covers ::getRecipients
	 */
	public function testRemoveRecipient() {
		$msg = new MandrillMessage;
		$all = $msg->getRecipients();
		$this->assertCount(0, $all);
		
		$this->assertSame($msg, $msg->removeRecipient('test@example.com'), 'removeRecipient() should be fluid');
		$this->assertSame($all, $msg->getRecipients());
		
		$msg->addRecipient('test@example.com')->addRecipient('foo@example.com');
		$this->assertCount(2, $msg->getRecipients());
		
		$this->assertSame($msg, $msg->removeRecipient('test@example.com'), 'removeRecipient() should be fluid');
		$this->assertCount(1, $msg->getRecipients());
		foreach($msg->getRecipients() as $spec) {
			$this->assertNotContains('test@example.com', $spec);
		}
	}
	
	/**
	 * @covers ::setFromName
	 * @covers ::setFromEmail
	 * @covers ::getFromName
	 * @covers ::getFromEmail
	 * @covers ::setFrom
	 * @covers ::clearFrom
	 */
	public function testSetGetFrom() {
		$msg = new MandrillMessage;
		$this->assertNull($msg->getFromName());
		$this->assertNull($msg->getFromEmail());
		
		$this->assertSame($msg, $msg->setFrom('test@example.com', 'Test'));
		$this->assertSame('test@example.com', $msg->getFromEmail());
		$this->assertSame('Test', $msg->getFromName());
		$this->assertSame($msg, $msg->clearFrom());
		$this->assertNull($msg->getFromName());
		$this->assertNull($msg->getFromEmail());
	}
	
	/**
	 * @covers ::setTemplateSlug
	 * @covers ::getTemplateSlug
	 * @covers ::usesTemplate
	 */
	public function testSetGetTemplate() {
		$msg = new MandrillMessage;
		$this->assertNull($msg->getTemplateSlug());
		$this->assertFalse($msg->usesTemplate());
		$this->assertSame($msg, $msg->setTemplateSlug('test'));
		$this->assertSame('test', $msg->getTemplateSlug());
		$this->assertTrue($msg->usesTemplate());
		$msg->setTemplateSlug('');
		$this->assertNull($msg->getTemplateSlug());
		$this->assertFalse($msg->usesTemplate());
	}
	
	/**
	 * @covers ::setAsync
	 * @covers ::isAsync
	 */
	public function testSetIsAsync() {
		$msg = new MandrillMessage;
		$this->assertFalse($msg->isAsync());
		$this->assertSame($msg, $msg->setAsync(true));
		$this->assertTrue($msg->isAsync());
		$msg->setAsync(false);
		$this->assertFalse($msg->isAsync());
		
		for($i = 0; $i <= 11; $i++) {
			$msg->addRecipient('test'.$i.'@example.com');
		}
		
		$this->assertTrue($msg->isAsync());
	}
	
	/**
	 * @covers ::getGoogleAnalyticsDomains
	 * @covers ::setGoogleAnalyticsDomains
	 * @covers ::addGoogleAnalyticsDomain
	 * @covers ::removeGoogleAnalyticsDomain
	 */
	public function testGaDomains() {
		$msg = new MandrillMessage;
		$this->assertInternalType('array', $msg->getGoogleAnalyticsDomains());
		$this->assertCount(0, $msg->getGoogleAnalyticsDomains());
		$this->assertSame($msg, $msg->addGoogleAnalyticsDomain('TEST2.com'));
		$this->assertCount(1, $msg->getGoogleAnalyticsDomains());
		$this->assertSame($msg, $msg->removeGoogleAnalyticsDomain('TEST2.com'));
		$this->assertCount(0, $msg->getGoogleAnalyticsDomains());
		
		$msg = new MandrillMessage;
		// This is just for coverage:
		$this->assertSame($msg, $msg->removeGoogleAnalyticsDomain('TEST2.com'));
		
		$domains = array('TEST.COM', 'TEST2.COM');
		$this->assertSame($msg, $msg->setGoogleAnalyticsDomains($domains));
		$expect = array('test.com', 'test2.com');
		$this->assertSame($expect, $msg->getGoogleAnalyticsDomains());
		
		$this->assertSame($msg, $msg->removeGoogleAnalyticsDomain('TEST2.com'));
		
		$expect = array('test.com');
		foreach($expect as $domain) {
			$this->assertContains($domain, $msg->getGoogleAnalyticsDomains());
		}
		
		$this->assertSame($msg, $msg->addGoogleAnalyticsDomain('TEST3.com'));
		$expect = array('test.com', 'test3.com');
		foreach($expect as $domain) {
			$this->assertContains($domain, $msg->getGoogleAnalyticsDomains());
		}
	}
	
	/**
	 * @covers ::setTagValidator
	 * @covers ::getTagValidator
	 */
	public function testGetSetTagValidator() {
		$msg = new MandrillMessage;
		$v = $msg->getTagValidator();
		$this->assertInstanceOf('NetglueMandrillModule\Validator\MandrillTag', $v);
		
		$v = new TagValidator;
		$this->assertSame($msg, $msg->setTagValidator($v));
		$this->assertSame($v, $msg->getTagValidator());
	}
	
	/**
	 * @covers ::tag
	 * @covers ::removeTag
	 * @covers ::clearTags
	 * @covers ::setTags
	 * @covers ::getTags
	 */
	public function testTagging() {
		$msg = new MandrillMessage;
		$this->assertInternalType('array', $msg->getTags());
		$this->assertCount(0, $msg->getTags());
		
		$this->assertSame($msg, $msg->removeTag('test'), 'Remove non-existent tag should be fluid');
		
		$this->assertSame($msg, $msg->tag(''), 'Add empty tag should return self');
		$this->assertCount(0, $msg->getTags(), 'Empty tags should not get registered');
		
		$this->assertSame($msg, $msg->tag('test'));
		$this->assertCount(1, $msg->getTags());
		$this->assertContains('test', $msg->getTags());
		
		$this->assertSame($msg, $msg->removeTag('test'));
		$this->assertCount(0, $msg->getTags());
		
		$tags = array('one', 'two', 'three');
		$this->assertSame($msg, $msg->setTags($tags));
		$this->assertSame($tags, $msg->getTags());
		
		$msg->tag('two');
		$this->assertSame($tags, $msg->getTags(), 'Identical tags should not get added twice');
		
		$this->assertSame($msg, $msg->clearTags());
		$this->assertCount(0, $msg->getTags());
	}
	
	/**
	 * @covers ::tag
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage The tag '_test' is not valid
	 */
	public function testInvalidTagThrowsException() {
		$msg = new MandrillMessage;
		$msg->tag('_test');
	}
	
	/**
	 * @covers ::setOpenTracking
	 * @covers ::isOpenTracking
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 */
	public function testOpenTrackingFlag() {
		$msg = new MandrillMessage;
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('track_opens', $config), 'Open tracking should not yet be defined in config');
		
		$this->assertNull($msg->isOpenTracking(), 'Open tracking flag should be initially null');
		$this->assertSame($msg, $msg->setOpenTracking(true), 'setOpenTracking() should be fluid');
		$this->assertTrue($msg->isOpenTracking(), 'OpenTracking flag should now be true');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('track_opens', $config, 'track_opens should be set in the config array');
		$msg->setOpenTracking(false);
		$this->assertFalse($msg->isOpenTracking());
		$msg->setOpenTracking(NULL);
		$this->assertNull($msg->isOpenTracking(), 'We should be able to unset the open tracking flag');
	}
	
	/**
	 * @covers ::setBoolParam
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Unknown property name
	 */
	public function testSetBoolParamThrowsExceptionForInvalidParamName() {
		$msg = new MandrillMessage;
		$msg->setBoolParam('foo', false);
	}
	
	/**
	 * @covers ::getBoolParam
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Unknown property name
	 */
	public function testGetBoolParamThrowsExceptionForInvalidParamName() {
		$msg = new MandrillMessage;
		$msg->getBoolParam('foo');
	}
	
	/**
	 * @covers ::setPreserveRecipients
	 * @covers ::isPreserveRecipients
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 */
	public function testPreserveRecipientsFlag() {
		$msg = new MandrillMessage;
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('preserve_recipients', $config), 'preserve_recipients should not yet be defined in config');
		
		$this->assertNull($msg->isPreserveRecipients(), 'preserve_recipients flag should be initially null');
		$this->assertSame($msg, $msg->setPreserveRecipients(true), 'setPreserveRecipients() should be fluid');
		$this->assertTrue($msg->isPreserveRecipients(), 'PreserveRecipients flag should now be true');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('preserve_recipients', $config, 'preserve_recipients should be set in the config array');
		$msg->setPreserveRecipients(false);
		$this->assertFalse($msg->isPreserveRecipients());
		$msg->setPreserveRecipients(NULL);
		$this->assertNull($msg->isPreserveRecipients(), 'We should be able to unset the preserve_recipients flag');
	}
	
	/**
	 * @covers ::setStripQueryStringForTrackingStats
	 * @covers ::isQueryStringStrippedForTrackingStats
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 */
	public function testSetStripQueryStringForTrackingStatsFlag() {
		$msg = new MandrillMessage;
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('url_strip_qs', $config), 'url_strip_qs should not yet be defined in config');
		
		$this->assertNull($msg->isQueryStringStrippedForTrackingStats(), 'url_strip_qs flag should be initially null');
		$this->assertSame($msg, $msg->setStripQueryStringForTrackingStats(true), 'setStripQueryStringForTrackingStats() should be fluid');
		$this->assertTrue($msg->isQueryStringStrippedForTrackingStats(), 'url_strip_qs flag should now be true');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('url_strip_qs', $config, 'url_strip_qs should be set in the config array');
		$msg->setStripQueryStringForTrackingStats(false);
		$this->assertFalse($msg->isQueryStringStrippedForTrackingStats());
		$msg->setStripQueryStringForTrackingStats(NULL);
		$this->assertNull($msg->isQueryStringStrippedForTrackingStats(), 'We should be able to unset the url_strip_qs flag');
	}
	
	/**
	 * @covers ::setClickTracking
	 * @covers ::isClickTracking
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 */
	public function testClickTrackingFlag() {
		$msg = new MandrillMessage;
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('track_clicks', $config), 'track_clicks should not yet be defined in config');
		
		$this->assertNull($msg->isClickTracking(), 'track_clicks flag should be initially null');
		$this->assertSame($msg, $msg->setClickTracking(true), 'setClickTracking() should be fluid');
		$this->assertTrue($msg->isClickTracking(), 'ClickTracking flag should now be true');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('track_clicks', $config, 'track_clicks should be set in the config array');
		$msg->setClickTracking(false);
		$this->assertFalse($msg->isClickTracking());
		$msg->setClickTracking(NULL);
		$this->assertNull($msg->isClickTracking(), 'We should be able to unset the track_clicks flag');
	}
	
	
	/**
	 * @covers ::setInlineCss
	 * @covers ::isInlineCss
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 */
	public function testInlineCssFlag() {
		$msg = new MandrillMessage;
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('inline_css', $config), 'inline_css should not yet be defined in config');
		
		$this->assertNull($msg->isInlineCss(), 'inline_css flag should be initially null');
		$this->assertSame($msg, $msg->setInlineCss(true), 'setInlineCss() should be fluid');
		$this->assertTrue($msg->isInlineCss(), 'InlineCss flag should now be true');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('inline_css', $config, 'inline_css should be set in the config array');
		$msg->setInlineCss(false);
		$this->assertFalse($msg->isInlineCss());
		$msg->setInlineCss(NULL);
		$this->assertNull($msg->isInlineCss(), 'We should be able to unset the inline_css flag');
	}
	
	/**
	 * @covers ::setImportant
	 * @covers ::isImportant
	 * @covers ::setBoolParam
	 * @covers ::getBoolParam
	 */
	public function testImportantFlag() {
		$msg = new MandrillMessage;
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('important', $config), 'important should not yet be defined in config');
		
		$this->assertNull($msg->isImportant(), 'important flag should be initially null');
		$this->assertSame($msg, $msg->setImportant(true), 'setImportant() should be fluid');
		$this->assertTrue($msg->isImportant(), 'Important flag should now be true');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('important', $config, 'important should be set in the config array');
		$msg->setImportant(false);
		$this->assertFalse($msg->isImportant());
		$msg->setImportant(NULL);
		$this->assertNull($msg->isImportant(), 'We should be able to unset the important flag');
	}
	
	/**
	 * @covers ::setHtml
	 * @covers ::getHtml
	 */
	public function testSetGetHtml() {
		$msg = new MandrillMessage;
		$this->assertNull($msg->getHtml(), 'html content should be initially null');
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('html', $config), 'HTML should not yet be defined in config');
		
		$this->assertSame($msg, $msg->setHtml('TEST'), 'setHtml() should be fluid');
		$this->assertSame('TEST', $msg->getHtml(), 'getHtml() should return the same content set');
		
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('html', $config, 'HTML content should be set in the config array');
		
		$msg->setHtml('');
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('html', $config), 'An empty html content param should unset the config value');
		$this->assertNull($msg->getHtml(), 'html content should now be null');
	}
	
	/**
	 * @covers ::setText
	 * @covers ::getText
	 */
	public function testSetGetText() {
		$msg = new MandrillMessage;
		$this->assertNull($msg->getText(), 'text content should be initially null');
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('text', $config), 'Text should not yet be defined in config');
		
		$this->assertSame($msg, $msg->setText('TEST'), 'setText() should be fluid');
		$this->assertSame('TEST', $msg->getText(), 'getText() should return the same content set');
		
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('text', $config, 'Text content should be set in the config array');
		
		$msg->setText('');
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('text', $config), 'An empty text content param should unset the config value');
		$this->assertNull($msg->getText(), 'text content should now be null');
	}
	
	/**
	 * @covers ::setGlobalMergeVar
	 * @covers ::getGlobalMergeVar
	 * @covers ::getGlobalMergeVars
	 */
	public function testSetGetGlobalMergeVar() {
		$msg = new MandrillMessage;
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('global_merge_vars', $config));
		$this->assertInternalType('array', $msg->getGlobalMergeVars());
		$this->assertNull($msg->getGlobalMergeVar('test'), 'Unknown merge vars should return null');
		$this->assertSame($msg, $msg->setGlobalMergeVar('test', 'foo'), 'setGlobalMergeVar() should be fluid');
		$this->assertSame('foo', $msg->getGlobalMergeVar('test'), 'We should be reciving the same value back');
		$config = $msg->getMessageConfig();
		$this->assertArrayHasKey('global_merge_vars', $config);
		$this->assertCount(1, $config['global_merge_vars']);
		$spec = current($config['global_merge_vars']);
		$this->assertCount(2, $spec);
		$this->assertArrayHasKey('name', $spec);
		$this->assertArrayHasKey('content', $spec);
		$this->assertSame('TEST', $spec['name']);
		$this->assertSame('foo', $spec['content']);
		
		$msg->setGlobalMergeVar('test', 'bar');
		$this->assertSame('bar', $msg->getGlobalMergeVar('TEST'));
		$this->assertNull($msg->getGlobalMergeVar('unknown'));
		
		$vars = $msg->getGlobalMergeVars();
		$this->assertInternalType('array', $vars);
		$this->assertCount(1, $vars);
		$this->assertSame(array(
			'TEST' => 'bar',
		), $vars);
	}
	
	/**
	 * @covers ::setGlobalMergeVar
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Merge variable name should be a string
	 */
	public function testSetGlobalMergeVarThrowsOnInvalidName() {
		$msg = new MandrillMessage;
		$msg->setGlobalMergeVar(100, 'foo');
	}
	
	/**
	 * @covers ::setGlobalMergeVar
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Merge variable value should be a scalar
	 */
	public function testSetGlobalMergeVarThrowsOnInvalidValue() {
		$msg = new MandrillMessage;
		$msg->setGlobalMergeVar('test', array());
	}
	
	/**
	 * @covers ::getGlobalMergeVar
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Merge variable name should be a string
	 */
	public function testGetGlobalMergeVarThrowsOnInvalidName() {
		$msg = new MandrillMessage;
		$msg->getGlobalMergeVar(100);
	}
	
	/**
	 * @covers ::setGlobalMergeVars
	 */
	public function testSetGlobalMergeVars() {
		$msg = new MandrillMessage;
		$vars = array(
			'foo' => 'bar',
			'test' => 'test',
			'bing' => 'BONG',
			'blah' => NULL,
		);
		$expect = array(
			'FOO' => 'bar',
			'TEST' => 'test',
			'BING' => 'BONG',
			'BLAH' => '',
		);
		$this->assertSame($msg, $msg->setGlobalMergeVars($vars));
		$this->assertSame($expect, $msg->getGlobalMergeVars());
	}
	
	/**
	 * @covers ::setGlobalMergeVars
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage setGloablMergeVars() Expects an array or Traversable
	 */
	public function testSetGlobalMergeVarsThrowsExceptionForIncorrectType() {
		$msg = new MandrillMessage;
		$msg->setGlobalMergeVars('foo');
	}
	
	/**
	 * @covers ::normaliseHeaderName
	 */
	public function testNormaliseHeaderName() {
		$input = array(
			'Some header-name' => 'Some-Header-Name',
			'x-Foo-bar' => 'X-Foo-Bar',
			'oomfoofOO' => 'OomfoofOO',
			'One_two_buckle_my_shoe' => 'One-Two-Buckle-My-Shoe',
		);
		$msg = new MandrillMessage;
		foreach($input as $header => $expect) {
			$this->assertSame($expect, $msg->normaliseHeaderName($header));
		}
	}
	
	/**
	 * @covers ::normaliseHeaderName
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Header name must be a string
	 */
	public function testNormaliseHeaderNameThrowsExceptionForNonString() {
		$msg = new MandrillMessage;
		$msg->normaliseHeaderName(array());
	}
	
	/**
	 * @covers ::normaliseHeaderName
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage Header name must be composed of printable US-ASCII characters, except colon.
	 */
	public function testNormaliseHeaderNameThrowsExceptionForNonUsAscii() {
		$msg = new MandrillMessage;
		$msg->normaliseHeaderName('X-Foo-€');
	}
	
	/**
	 * @covers ::setHeaders
	 * @covers ::getHeaders
	 * @covers ::addHeader
	 * @covers ::removeHeader
	 * @covers ::clearHeaders
	 */
	public function testGetSetHeaders() {
		$msg = new MandrillMessage;
		
		$this->assertInternalType('array', $msg->getHeaders());
		$this->assertCount(0, $msg->getHeaders());
		
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('headers', $config));
		
		$this->assertSame($msg, $msg->addHeader('Header 1', 'Test'));
		$expect = array(
			'Header-1' => 'Test',
		);
		$this->assertSame($expect, $msg->getHeaders());
		
		$this->assertSame($msg, $msg->clearHeaders());
		$this->assertCount(0, $msg->getHeaders());
		
		$msg->addHeader('Header 1', 'Test');
		
		$input = array(
			'Some header-name' => 'Test',
			'x-Foo-bar' => 'Test',
			'oomfoofOO' => 'Test',
			'One_two_buckle_my_shoe' => 'Test',
		);
		
		$expect = array(
			'Some-Header-Name' => 'Test',
			'X-Foo-Bar' => 'Test',
			'OomfoofOO' => 'Test',
			'One-Two-Buckle-My-Shoe' => 'Test',
		);
		
		$this->assertSame($msg, $msg->setHeaders($input));
		$this->assertSame($expect, $msg->getHeaders());
		
		$msg->clearHeaders();
		
		$msg->addHeader('Foo', '       ');
		$expect = array(
			'Foo' => '',
		);
		$this->assertSame($expect, $msg->getHeaders());
		
		$this->assertSame($msg, $msg->removeHeader('Foo'));
		$config = $msg->getMessageConfig();
		$this->assertFalse(array_key_exists('headers', $config));
		
		// For coverage - removing headers when none set will just return self
		$this->assertSame($msg, $msg->removeHeader('Foo'));
	}
	
}
