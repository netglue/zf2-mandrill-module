<?php

namespace NetglueMandrillModuleTest\Message;

use PHPUnit_Framework_TestCase as TestCase;


use NetglueMandrillModule\Message\DeliveryResult;

/**
 * @coversDefaultClass NetglueMandrillModule\Message\DeliveryResult
 */
class DeliveryResultTest extends TestCase {
	
	/**
	 * @covers ::__construct
	 * @covers ::getFailureCount
	 * @covers ::getSuccessCount
	 * @covers ::getResultArray
	 * @covers ::isPartialSuccess
	 * @covers ::isSuccess
	 * @covers ::isPartialFailure
	 * @covers ::isFailure
	 * @covers ::hasFailure
	 * @covers ::getFailedEmails
	 * @covers ::getMessageId
	 * @covers ::getMessageStatus
	 * @covers ::getRejectionReason
	 * @covers ::getResponseForEmail
	 */
	public function testBasic() {
		$data = array(
			array(
				'email' => 'test',
				'_id' => 'testId',
				'status' => 'sent',
				'reject_reason' => NULL,
			),
			array(
				'email' => 'test2',
				'_id' => 'testId2',
				'status' => 'invalid',
				'reject_reason' => NULL,
			),
		);
		
		$dr = new DeliveryResult($data);
		$this->assertSame($data, $dr->getResultArray());
		$this->assertEquals(1, $dr->getFailureCount());
		$this->assertEquals(1, $dr->getSuccessCount());
		
		$this->assertTrue($dr->isPartialFailure());
		$this->assertTrue($dr->isPartialSuccess());
		$this->assertFalse($dr->isSuccess());
		$this->assertFalse($dr->isFailure());
		$this->assertTrue($dr->hasFailure());
		
		$expect = array(
			'test2',
		);
		
		$this->assertSame($expect, $dr->getFailedEmails());
		
		$this->assertSame($data[0], $dr->getResponseForEmail('test'));
		$this->assertSame($data[1], $dr->getResponseForEmail('test2'));
		
		$this->assertSame('testId', $dr->getMessageId('test'));
		$this->assertSame('testId2', $dr->getMessageId('test2'));
		
		$this->assertSame('sent', $dr->getMessageStatus('test'));
		$this->assertSame('invalid', $dr->getMessageStatus('test2'));
		
		
		$this->assertNull($dr->getRejectionReason('test'));
		$this->assertNull($dr->getRejectionReason('test2'));
		
	}
	
	/**
	 * @covers ::getResponseForEmail
	 */
	public function testGetResponseForEmailReturnsFalseForWrongEmail() {
		$data = array(
			array(
				'email' => 'test',
				'_id' => 'testId',
				'status' => 'sent',
				'reject_reason' => NULL,
			),
		);
		
		$dr = new DeliveryResult($data);
		
		$this->assertFalse($dr->getResponseForEmail('foo'));
		$this->assertSame($data[0], $dr->getResponseForEmail('test'));
	}
	
	/**
	 * @covers ::getMessageId
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage There is no response for the email address
	 */
	public function testGetMessageIdThrowsExceptionForUnknownEmail() {
		$data = array(
			array(
				'email' => 'test',
				'_id' => 'testId',
				'status' => 'sent',
				'reject_reason' => NULL,
			),
		);
		
		$dr = new DeliveryResult($data);
		$dr->getMessageId('foo');
	}
	
	/**
	 * @covers ::getMessageStatus
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage There is no response for the email address
	 */
	public function testGetMessageStatusThrowsExceptionForUnknownEmail() {
		$data = array(
			array(
				'email' => 'test',
				'_id' => 'testId',
				'status' => 'sent',
				'reject_reason' => NULL,
			),
		);
		
		$dr = new DeliveryResult($data);
		$dr->getMessageStatus('foo');
	}
	
	/**
	 * @covers ::getRejectionReason
	 * @expectedException NetglueMandrillModule\Exception\InvalidArgumentException
	 * @expectedExceptionMessage There is no response for the email address
	 */
	public function testGetRejectionReasonThrowsExceptionForUnknownEmail() {
		$data = array(
			array(
				'email' => 'test',
				'_id' => 'testId',
				'status' => 'sent',
				'reject_reason' => NULL,
			),
		);
		
		$dr = new DeliveryResult($data);
		$dr->getRejectionReason('foo');
	}
	
}