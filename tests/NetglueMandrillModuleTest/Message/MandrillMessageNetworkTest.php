<?php

namespace NetglueMandrillModuleTest\Message;

use PHPUnit_Framework_TestCase as TestCase;

use NetglueMandrillModuleTest\bootstrap;

use NetglueMandrillModule\Message\MandrillMessage;
use NetglueMandrill\Client\MandrillClient;
use NetglueMandrillModule\Validator\MandrillTag as TagValidator;

/**
 * @coversDefaultClass NetglueMandrillModule\Message\MandrillMessage
 */
class MandrillMessageNetworkTest extends TestCase {

	/**
	 * @covers ::send
	 * @expectedException NetglueMandrillModule\Exception\MessageErrorException
	 * @expectedExceptionMessage Message Failure
	 */
	public function testInvalidApiKeyException() {
		try {
			$client = new MandrillClient('InvalidKey');
			$msg = new MandrillMessage;
			$msg->setMandrillClient($client);
			$msg->send();
		} catch(\NetglueMandrillModule\Exception\MessageErrorException $e) {
			$prev = $e->getPrevious();
			$this->assertInstanceOf('NetglueMandrill\Exception\ExceptionInterface', $prev, 'Previous Caught exception should be from the API');
			throw $e;
		}
	}

	/**
	 * @covers ::send
	 * @covers NetglueMandrillModule\Factory\MandrillClientFactory::createService
	 * @covers NetglueMandrillModule\Factory\MandrillMessageFactory::createService
	 */
	public function testValidApiKeyAndSendReturnsDeliveryResult() {
		$sl = bootstrap::getServiceManager();
		$factory = $sl->get('NetglueMandrillModule\Factory\MandrillMessageFactory');
		$msg = $factory->createMessage();
		$this->assertInstanceOf('NetglueMandrillModule\Message\MandrillMessage', $msg);
		$this->assertInstanceOf('NetglueMandrill\Client\MandrillClient', $msg->getMandrillClient());

		$config = $sl->get('Config');
		$to = $config['netglue_mandrill']['testing']['to'];
		$msg->addRecipient($to);
		$msg->setFrom($config['netglue_mandrill']['testing']['from']);
		$msg->setText('Test Message');
		$result = $msg->send();
		$this->assertInstanceOf('NetglueMandrillModule\Message\DeliveryResult', $result);
		$data = $result->getResponseForEmail($to);
		$this->assertSame($to, $data['email']);
	}

}
