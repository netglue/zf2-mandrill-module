<?php

namespace NetglueMandrillModuleTest\Validator;

use PHPUnit_Framework_TestCase as TestCase;

use NetglueMandrillModule\Validator\MandrillTag as TagValidator;

/**
 * @coversDefaultClass NetglueMandrillModule\Validator\MandrillTag
 */
class MandrillTagTest extends TestCase {
	
	/**
	 * @covers ::isValid
	 */
	public function testBasic() {
		
		// Leading Underscores are not allowed:
		$v = new TagValidator;
		$this->assertFalse($v->isValid('_foo'));
		$expect = 'Tags cannot begin with an underscore. These tags are reserved for internal Mandrill usage';
		$this->assertContains($expect, $v->getMessages());
		
		// Must be string
		$v = new TagValidator;
		$this->assertFalse($v->isValid(array()));
		$expect = 'Tags should be alpha numeric strings';
		$this->assertContains($expect, $v->getMessages());
		
		// Must be no more than 50 chrs
		$v = new TagValidator;
		$fifty = str_repeat('a', 50);
		$this->assertTrue($v->isValid($fifty));
		
		$hundred = str_repeat('ab', 50);
		$this->assertFalse($v->isValid($hundred));
		$expect = 'Tags have a maximum of 50 characters';
		$this->assertContains($expect, $v->getMessages());
		
		$this->assertTrue($v->isValid('test'));
	}
	
}
