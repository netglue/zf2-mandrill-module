# Mandrill Module for ZF2

This module is intended to provide painless integration of the [Mandrill API](https://mandrillapp.com/api/docs/index.html)

The module is not feature complete but what's here is reasonably tested.

## Install

Use composer: `"netglue/zf2-mandrill-module" : "dev-develop"`

Add to modules with `NetglueMandrillModule`

## Configure

Consult `config/module.config.php`

## Available Services

* `'NetglueMandrill\Client\MandrillClient'` - An API Client configured with your API Key
* `'NetglueMandrillModule\Factory\MandrillMessageFactory'` - Creates Message instances with pre-configured defaults

Consult `config/
## Currently...

...this module provides a service so you can easily create pre-configured messages easily.
I use it primarily to populate template based messages like this:

    'myMessageName' => array(
        'template_name' => 'my-template-name-on-mandrill',
        'message' => array(
            'from' => array(
                'name' => 'Me!',
                'email' => 'me@example.com',
            ),
            'subject' => 'Template Based Message',
        ),
    ),
    // ...
    
    // Later on, somewhere in your app:
    
    $factory = $serviceLocator->get('NetglueMandrillModule\Factory\MandrillMessageFactory);
    $message = $factory->createMessage('myMessageName');
    $message->addRecipient('joe@example.com', 'Joe Bloggs', 'to');
    $deliveryResult = $message->send();
    var_dump($deliveryResult->isSuccess()); // true with any luck

## See Also

The [Guzzle backed API Client library](https://bitbucket.org/netglue/mandrill-api)


## Tests

Requires phpunit 3.7 ish. May well work with newer or older versions.

    $ cd path/to/module
    $ composer install
    $ cd tests
    $ nano|vi|whatever config/my.local.php
    $ phpunit

## Issues/Contributing

Contributions/fixes are much appreciated.

Add issues to the [tracker here](https://bitbucket.org/netglue/zf2-mandrill-module/issues)
