<?php

namespace NetglueMandrillModule\Message;
use NetglueMandrillModule\Exception;

class DeliveryResult
{
    /**
     * Decoded JSON Body as returned from API
     * @var array
     */
    protected $body;

    /**
     * These statuses indicate success
     * @var array
     */
    protected $successStatus = array(
        "sent",
        "queued",
        "scheduled",
    );

    /**
     * These status string indicate failure
     * @var array
     */
    protected $failedStatus = array(
        "rejected",
        "invalid",
    );

    /**
     * Failure count
     * @var int
     */
    protected $failCount = 0;

    /**
     * Success Count
     * @var int
     */
    protected $successCount = 0;

    /**
     * Constructor process result returned from api
     * @param  array $response
     * @return void
     */
    public function __construct($response)
    {
        $this->body = $response;
        foreach ($this->body as $spec) {
            if (in_array($spec['status'], $this->failedStatus)) {
                $this->failCount++;
            }
            if (in_array($spec['status'], $this->successStatus)) {
                $this->successCount++;
            }
        }
    }

    /**
     * Return the result array
     * @return array
     */
    public function getResultArray()
    {
        return $this->body;
    }

    /**
     * Return failure count
     * @return int
     */
    public function getFailureCount()
    {
        return $this->failCount;
    }

    /**
     * Return success count
     * @return int
     */
    public function getSuccessCount()
    {
        return $this->successCount;
    }

    /**
     * Partial Success?
     * returns false if all messages were successfully
     * @return bool
     */
    public function isPartialSuccess()
    {
        return ( $this->successCount > 0 && $this->failCount > 0 );
    }

    /**
     * Complete Success?
     * Only returns true if all messages/recipients were successful
     * @return bool
     */
    public function isSuccess()
    {
        return ( $this->successCount > 0 && $this->failCount === 0 );
    }

    /**
     * Partial Success?
     * Only returns true when some recipients succeeded
     * @return bool
     */
    public function isPartialFailure()
    {
        return ( $this->successCount > 0 && $this->failCount > 0 );
    }

    /**
     * Complete Failure
     * Only returns true when all recipients failed
     * @return bool
     */
    public function isFailure()
    {
        return ( $this->successCount === 0 && $this->failCount > 0 );
    }

    /**
     * Whether there are any failures at all
     * @return bool
     */
    public function hasFailure()
    {
        return $this->failCount > 0;
    }

    /**
     * Return the failed email addresses
     * @return array
     */
    public function getFailedEmails()
    {
        $out = array();
        foreach ($this->body as $spec) {
            if (in_array($spec['status'], $this->failedStatus)) {
                $out[] = $spec['email'];
            }
        }

        return $out;
    }

    /**
     * Return message id for given recipient
     * @param  string $email
     * @return string
     */
    public function getMessageId($email)
    {
        $spec = $this->getResponseForEmail($email);
        if (!$spec) {
            throw new Exception\InvalidArgumentException("There is no response for the email address {$email}");
        }

        return $spec['_id'];
    }

    /**
     * Return status for given recipient
     * @param  string $email
     * @return string
     */
    public function getMessageStatus($email)
    {
        $spec = $this->getResponseForEmail($email);
        if (!$spec) {
            throw new Exception\InvalidArgumentException("There is no response for the email address {$email}");
        }

        return $spec['status'];
    }

    /**
     * Return Rejection reason for given recipient
     * @param  string $email
     * @return string
     */
    public function getRejectionReason($email)
    {
        $spec = $this->getResponseForEmail($email);
        if (!$spec) {
            throw new Exception\InvalidArgumentException("There is no response for the email address {$email}");
        }

        return $spec['reject_reason'];
    }

    /**
     * Return full response for given recipient
     * @param  string      $email
     * @return array|false
     */
    public function getResponseForEmail($email)
    {
        $email = trim(strtolower($email));
        foreach ($this->body as $spec) {
            if ($spec['email'] === $email) {
                reset($this->body);

                return $spec;
            }
        }

        return false;
    }

}
