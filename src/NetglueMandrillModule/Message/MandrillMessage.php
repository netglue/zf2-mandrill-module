<?php

namespace NetglueMandrillModule\Message;

use NetglueMandrill\Client\MandrillClient;
use NetglueMandrillModule\Exception;
use NetglueMandrillModule\Validator\MandrillTag as TagValidator;

use NetglueMandrill\Exception\ExceptionInterface as ApiException;

use Zend\Validator\ValidatorInterface;

use Traversable;

class MandrillMessage
{
    /**
     * This is the limit on the number of recipients before Mandrill automatically goes async
     */
    const ASYNC_LIMIT = 10;

    /**
     * @var MandrillClient
     */
    protected $client;

    /**
     * Parameters given to the api client to send the message
     * @var array
     */
    protected $config = array();

    protected $validTopLevelKeys = array(
        'template_name',
        'template_content',
        'message',
        'async',
        'ip_pool',
    );

    /**
     * Template slug to use a template if required
     * @var string|NULL
     */
    protected $templateSlug;

    /**
     * Template Content
     * @var array
     */
    protected $templateContent = array();

    /**
     * Whether to send in async mode
     * @var bool
     */
    protected $async = false;

    /**
     * Validator for Tags
     * @var ValidatorInterface
     */
    protected $tagValidator;

    /**
     * Parameters that can all either be NULL or boolean
     * @var array
     */
    protected $booleanValues = array(
        'important',
        'track_opens',
        'track_clicks',
        'auto_text',
        'auto_html',
        'inline_css',
        'url_strip_qs',
        'preserve_recipients',
        'view_content_link',
    );

    /**
     * Delivery Result of the  Message
     * @var DeliveryResult
     */
    protected $deliveryResult;

    /**
     * Valid Recipient Types
     * @var array
     */
    protected static $validRecipientTypes = array('to', 'cc', 'bcc');

    /**
     * Send the message
     * @return self
     * @throws Exception\MessageErrorException if sending the message fails due to a error at the API level
     */
    public function send()
    {
        /**
         * Prepare Params for Sending
         */
        $config = array(
            'message' => $this->config,
            'async' => $this->async,
        );
        if ($this->usesTemplate()) {
            $config['template_name'] = $this->getTemplateSlug();
            // Template Content is Required but an empty array is fine
            $config['template_content'] = $this->templateContent;
        }

        try {
            if ($this->usesTemplate()) {
                $response = $this->getMandrillClient()->sendTemplate($config);
            } else {
                $response = $this->getMandrillClient()->sendMessage($config);
            }
            $this->deliveryResult = new DeliveryResult($response);

            return $this->deliveryResult;
        } catch (ApiException $apiException) {
            throw new Exception\MessageErrorException('Message Failure', NULL, $apiException);
        }
    }

    /**
     * Set Html Content of the message
     * @param  string $html
     * @return self
     */
    public function setHtml($html)
    {
        if (empty($html)) {
            unset($this->config['html']);

            return $this;
        }
        $this->config['html'] = (string) $html;

        return $this;
    }

    /**
     * Return HTML content
     * @return string|NULL
     */
    public function getHtml()
    {
        if (!isset($this->config['html'])) {
            return NULL;
        }

        return $this->config['html'];
    }

    /**
     * Set Text Content of the message
     * @param  string $text
     * @return self
     */
    public function setText($text)
    {
        if (empty($text)) {
            unset($this->config['text']);

            return $this;
        }
        $this->config['text'] = (string) $text;

        return $this;
    }

    /**
     * Set the value of a global merge variable
     * @param  string                             $name
     * @param  scalar                             $value
     * @return self
     * @throws Exception\InvalidArgumentException
     */
    public function setGlobalMergeVar($name, $value)
    {
        if (!is_string($name)) {
            throw new Exception\InvalidArgumentException('Merge variable name should be a string');
        }
        if (!is_scalar($value) && !is_null($value)) {
            throw new Exception\InvalidArgumentException('Merge variable value should be a scalar');
        }

        // Merge tags are case insensitive but commonly shown in uppercase
        $name = trim(strtoupper($name));
        // Values must be cast to a string
        $value = (string) $value;

        if (!isset($this->config['global_merge_vars'])) {
            $this->config['global_merge_vars'] = array();
        }
        foreach ($this->config['global_merge_vars'] as $key => $spec) {
            if ($spec['name'] === $name) {
                $this->config['global_merge_vars'][$key]['content'] = $value;
                reset($this->config['global_merge_vars']);

                return $this;
            }
        }
        $this->config['global_merge_vars'][] = array(
            'name' => $name,
            'content' => $value,
        );

        return $this;
    }

    /**
     * Return the value of a global merge var
     * @param  string                             $name
     * @return string|NULL
     * @throws Exception\InvalidArgumentException
     */
    public function getGlobalMergeVar($name)
    {
        if (!is_string($name)) {
            throw new Exception\InvalidArgumentException('Merge variable name should be a string');
        }
        $name = trim(strtoupper($name));
        if (!isset($this->config['global_merge_vars'])) {
            return NULL;
        }
        foreach ($this->config['global_merge_vars'] as $spec) {
            if ($spec['name'] === $name) {
                reset($this->config['global_merge_vars']);

                return $spec['content'];
            }
        }

        return NULL;
    }

    /**
     * Return the global Merge vars currently set as an associative array
     * @return array
     */
    public function getGlobalMergeVars()
    {
        if (!isset($this->config['global_merge_vars'])) {
            return array();
        }
        $out = array();
        foreach ($this->config['global_merge_vars'] as $spec) {
            $name = $spec['name'];
            $value = $spec['content'];
            $out[$name] = $value;
        }

        return $out;
    }

    /**
     * Set All global merge vars at once overwriting those previously set
     * @param  array|Traversable                  $data
     * @return self
     * @throws Exception\InvalidArgumentException
     */
    public function setGlobalMergeVars($data)
    {
        if (!is_array($data) && ! $data instanceof Traversable) {
            throw new Exception\InvalidArgumentException('setGloablMergeVars() Expects an array or Traversable');
        }
        $this->config['global_merge_vars'] = array();
        foreach ($data as $name => $value) {
            $this->setGlobalMergeVar($name, $value);
        }

        return $this;
    }

    /**
     * Set the value of a recipient merge variable
     * @param  string                             $email
     * @param  string                             $name
     * @param  scalar                             $value
     * @return self
     * @throws Exception\InvalidArgumentException
     */
    public function setRecipientMergeVar($email, $name, $value)
    {
        if (!is_string($email)) {
            throw new Exception\InvalidArgumentException('Merge variable email address should be a string');
        }
        if (!is_string($name)) {
            throw new Exception\InvalidArgumentException('Merge variable name should be a string');
        }
        if (!is_scalar($value) && !is_null($value)) {
            throw new Exception\InvalidArgumentException('Merge variable value should be a scalar');
        }
        $name = trim(strtoupper($name));
        $email = trim(strtolower($email));
        $value = (string) $value;
        if (!isset($this->config['merge_vars'])) {
            $this->config['merge_vars'] = array();
        }
        if (!isset($this->config['merge_vars'][$email])) {
            $this->config['merge_vars'][$email] = array();
        }
        foreach ($this->config['merge_vars'][$email] as $key => $spec) {
            if ($spec['name'] === $name) {
                $this->config['merge_vars'][$email][$key]['content'] = $value;
                reset($this->config['merge_vars']);

                return $this;
            }
        }
        $this->config['merge_vars'][$email][] = array(
            'name' => $name,
            'content' => $value,
        );

        return $this;
    }

    /**
     * Return the value of a recipient merge var
     * @param  string                             $email
     * @param  string                             $name
     * @return string|NULL
     * @throws Exception\InvalidArgumentException
     */
    public function getRecipientMergeVar($email, $name)
    {
        if (!is_string($email)) {
            throw new Exception\InvalidArgumentException('Merge variable email address should be a string');
        }
        if (!is_string($name)) {
            throw new Exception\InvalidArgumentException('Merge variable name should be a string');
        }
        $name = trim(strtoupper($name));
        $email = trim(strtolower($email));
        if (!isset($this->config['merge_vars'][$email])) {
            return NULL;
        }
        foreach ($this->config['merge_vars'][$email] as $spec) {
            if ($spec['name'] === $name) {
                reset($this->config['merge_vars'][$email]);

                return $spec['content'];
            }
        }

        return NULL;
    }

    /**
     * Return recipient merge vars as an associate array
     * @param  string                             $email
     * @return array
     * @throws Exception\InvalidArgumentException
     */
    public function getRecipientMergeVars($email)
    {
        if (!is_string($email)) {
            throw new Exception\InvalidArgumentException('Merge variable email address should be a string');
        }
        $email = trim(strtolower($email));
        if (!isset($this->config['merge_vars'][$email])) {
            return array();
        }
        $out = array();
        foreach ($this->config['merge_vars'][$email] as $spec) {
            $name = $spec['name'];
            $value = $spec['content'];
            $out[$name] = $value;
        }

        return $out;
    }

    /**
     * Set All recipient merge vars at once overwriting those previously set
     * @param  string                             $email
     * @param  array|Traversable                  $data
     * @return self
     * @throws Exception\InvalidArgumentException
     */
    public function setRecipientMergeVars($email, $data)
    {
        if (!is_string($email)) {
            throw new Exception\InvalidArgumentException('Merge variable email address should be a string');
        }
        if (!is_array($data) && ! $data instanceof Traversable) {
            throw new Exception\InvalidArgumentException('setGloablMergeVars() Expects an array or Traversable');
        }
        $email = trim(strtolower($email));
        if (!isset($this->config['merge_vars'])) {
            $this->config['merge_vars'] = array();
        }
        if (!isset($this->config['merge_vars'][$email])) {
            $this->config['merge_vars'][$email] = array();
        }
        foreach ($data as $name => $value) {
            $this->setRecipientMergeVar($email, $name, $value);
        }

        return $this;
    }

    /**
     * Return Text content
     * @return string|NULL
     */
    public function getText()
    {
        if (!isset($this->config['text'])) {
            return NULL;
        }

        return $this->config['text'];
    }

    /**
     * Add an arbitrary message header
     * @param  string $fieldName
     * @param  string $fieldValue
     * @return self
     */
    public function addHeader($fieldName, $fieldValue)
    {
        $fieldName = $this->normaliseHeaderName($fieldName);

        $fieldValue = (string) $fieldValue;
        if (empty($fieldValue) || preg_match('/^\s+$/', $fieldValue)) {
            $fieldValue = '';
        }

        if (!isset($this->config['headers'])) {
            $this->config['headers'] = array();
        }

        $this->config['headers'][$fieldName] = $fieldValue;

        return $this;
    }

    /**
     * Clear all custom headers
     * @return self
     */
    public function clearHeaders()
    {
        unset($this->config['headers']);

        return $this;
    }

    /**
     * Set message headers with an associative array
     * @param  array $headers
     * @return self
     */
    public function setHeaders(array $headers)
    {
        $this->clearHeaders();
        foreach ($headers as $name => $value) {
            $this->addHeader($name, $value);
        }

        return $this;
    }

    /**
     * Return headers
     * @return array
     */
    public function getHeaders()
    {
        if (!isset($this->config['headers'])) {
            return array();
        }

        return $this->config['headers'];
    }

    /**
     * Remove single header
     * @param  string $name
     * @return self
     */
    public function removeHeader($name)
    {
        if (!isset($this->config['headers'])) {
            return $this;
        }
        $name = $this->normaliseHeaderName($name);
        unset($this->config['headers'][$name]);
        if (!count($this->config['headers'])) {
            $this->clearHeaders();
        }

        return $this;
    }

    /**
     * Normalise header name and throw exceptions for bad names
     * @param  string                             $fieldName
     * @return string
     * @throws Exception\InvalidArgumentException
     */
    public function normaliseHeaderName($fieldName)
    {
        /**
         * @see Zend\Mail\Header\GenericHeader
         */
        if (!is_string($fieldName) || empty($fieldName)) {
            throw new Exception\InvalidArgumentException('Header name must be a string');
        }
        // Pre-filter to normalize valid characters, change underscore to dash
        $fieldName = str_replace(' ', '-', ucwords(str_replace(array('_', '-'), ' ', $fieldName)));
        // Validate what we have
        if (!preg_match('/^[\x21-\x39\x3B-\x7E]*$/', $fieldName)) {
            throw new Exception\InvalidArgumentException('Header name must be composed of printable US-ASCII characters, except colon.');
        }

        return $fieldName;
    }

    /**
     * Set/Unset Important Flag
     * @param  bool|NULL $flag
     * @return self
     */
    public function setImportant($flag)
    {
        return $this->setBoolParam('important', $flag);
    }

    /**
     * Whether the message is flagged important
     * @return bool|NULL
     */
    public function isImportant()
    {
        return $this->getBoolParam('important');
    }

    /**
     * Set/Unset Open Tracking Flag
     * @param  bool|NULL $flag
     * @return self
     */
    public function setOpenTracking($flag)
    {
        return $this->setBoolParam('track_opens', $flag);
    }

    /**
     * Whether the message is set to track opens
     * @return bool|NULL
     */
    public function isOpenTracking()
    {
        return $this->getBoolParam('track_opens');
    }

    /**
     * Set/Unset Click Tracking Flag
     * @param  bool|NULL $flag
     * @return self
     */
    public function setClickTracking($flag)
    {
        return $this->setBoolParam('track_clicks', $flag);
    }

    /**
     * Whether the message is set to track clicks
     * @return bool|NULL
     */
    public function isClickTracking()
    {
        return $this->getBoolParam('track_clicks');
    }

    /**
     * Set/Unset Inline CSS Flag
     * @param  bool|NULL $flag
     * @return self
     */
    public function setInlineCss($flag)
    {
        return $this->setBoolParam('inline_css', $flag);
    }

    /**
     * Whether the message will try to inline CSS styles
     * @return bool|NULL
     */
    public function isInlineCss()
    {
        return $this->getBoolParam('inline_css');
    }

    /**
     * Set/Unset flag to determine whether the query string should be stripped from urls when gathering click tracking data
     * @param  bool|NULL $flag
     * @return self
     */
    public function setStripQueryStringForTrackingStats($flag)
    {
        return $this->setBoolParam('url_strip_qs', $flag);
    }

    /**
     * Whether the query strings will be stripped for click tracking data
     * @return bool|NULL
     */
    public function isQueryStringStrippedForTrackingStats()
    {
        return $this->getBoolParam('url_strip_qs');
    }

    /**
     * Set/Unset Preserve Recipients Flag
     * Setting to true means that all to and cc recipients will be exposed to each other in message headers
     * @param  bool|NULL $flag
     * @return self
     */
    public function setPreserveRecipients($flag)
    {
        return $this->setBoolParam('preserve_recipients', $flag);
    }

    /**
     * Whether mandrill will preserve recipient headers
     * @return bool|NULL
     */
    public function isPreserveRecipients()
    {
        return $this->getBoolParam('preserve_recipients');
    }

    /**
     * Enable View Content Flag
     * Setting to false will disable the 'View Content' link on the Mandrill App. Helpful for sensitive emails
     * @return self
     */
    public function enableContentView()
    {
        return $this->setBoolParam('view_content_link', true);
    }

    /**
     * Disable View Content Flag
     * Setting to false will disable the 'View Content' link on the Mandrill App. Helpful for sensitive emails
     * @return self
     */
    public function disableContentView()
    {
        return $this->setBoolParam('view_content_link', false);
    }

    /**
     * Whether View Content is enabled on the mandrill dashboard for this message
     * @return bool|NULL
     */
    public function isContentViewEnabled()
    {
        $flag = $this->getBoolParam('view_content_link');
        if (NULL === $flag) {
            return true; // By default this is enabled
        }

        return $flag;
    }

    /**
     * Return tag validator
     * @return ValidatorInterface
     */
    public function getTagValidator()
    {
        if (!$this->tagValidator) {
            $validator = new TagValidator;
            $this->setTagValidator($validator);
        }

        return $this->tagValidator;
    }

    /**
     * Set Alternative Tag Validator
     * @param  ValidatorInterface $validator
     * @return self
     */
    public function setTagValidator(ValidatorInterface $validator)
    {
        $this->tagValidator = $validator;

        return $this;
    }

    /**
     * Add a tag to the message
     * @param  string $tag
     * @return self
     */
    public function tag($tag)
    {
        // Skip empty tags without error
        if (empty($tag)) {
            return $this;
        }
        $validator = $this->getTagValidator();
        if (!$validator->isValid($tag)) {
            throw new Exception\InvalidArgumentException("The tag '{$tag}' is not valid: ".implode(', ', $validator->getMessages()));
        }
        $tag = trim($tag);

        if (!isset($this->config['tags'])) {
            $this->config['tags'] = array();
        }

        if (!in_array($tag, $this->config['tags'])) {
            $this->config['tags'][] = $tag;
        }

        return $this;
    }

    /**
     * Remove a tag from the message
     * @param  string $tag
     * @return self
     */
    public function removeTag($tag)
    {
        if (!isset($this->config['tags'])) {
            return $this;
        }
        $tag = trim($tag);
        if (in_array($tag, $this->config['tags'])) {
            $tags = array_flip($this->config['tags']);
            $key = $tags[$tag];
            unset($this->config['tags'][$key]);
        }
        if (!count($this->config['tags'])) {
            unset($this->config['tags']);
        }

        return $this;
    }

    /**
     * Clear message tags
     * @return self
     */
    public function clearTags()
    {
        unset($this->config['tags']);

        return $this;
    }

    /**
     * Set tags with an array
     * @param  array $tags
     * @return self
     */
    public function setTags(array $tags)
    {
        $this->clearTags();
        foreach ($tags as $tag) {
            $this->tag($tag);
        }

        return $this;
    }

    /**
     * Return tags
     * @return array
     */
    public function getTags()
    {
        if (!isset($this->config['tags'])) {
            return array();
        }

        return $this->config['tags'];
    }

    /**
     * Add a domain name to have GA tracking appended to any matching URLs
     * @param  string $domain
     * @return self
     */
    public function addGoogleAnalyticsDomain($domain)
    {
        $domain = trim(strtolower($domain));
        if (!isset($this->config['google_analytics_domains'])) {
            $this->config['google_analytics_domains'] = array();
        }
        if (!in_array($domain, $this->config['google_analytics_domains'])) {
            $this->config['google_analytics_domains'][] = $domain;
        }

        return $this;
    }

    /**
     * Remove GA tracking for links to the given domain
     * @param  string $domain
     * @return self
     */
    public function removeGoogleAnalyticsDomain($domain)
    {
        $domain = trim(strtolower($domain));
        if (!isset($this->config['google_analytics_domains'])) {
            return $this;
        }
        if (in_array($domain, $this->config['google_analytics_domains'])) {
            $domains = array_flip($this->config['google_analytics_domains']);
            $key = $domains[$domain];
            unset($this->config['google_analytics_domains'][$key]);
        }
        if (!count($this->getGoogleAnalyticsDomains())) {
            unset($this->config['google_analytics_domains']);
        }

        return $this;
    }

    /**
     * Return GA Domains setup for tracking
     * @return array
     */
    public function getGoogleAnalyticsDomains()
    {
        if (!isset($this->config['google_analytics_domains'])) {
            return array();
        }

        return $this->config['google_analytics_domains'];
    }

    /**
     * Set GA domains to track links to
     * @param  array $domains
     * @return self
     */
    public function setGoogleAnalyticsDomains(array $domains)
    {
        unset($this->config['google_analytics_domains']);
        foreach ($domains as $domain) {
            $this->addGoogleAnalyticsDomain($domain);
        }

        return $this;
    }

    /**
     * Turn on/off async mode
     * @param  bool $flag
     * @return self
     */
    public function setAsync($flag)
    {
        $this->async = (bool) $flag;

        return $this;
    }

    /**
     * Whether we are in async mode
     * @return bool
     */
    public function isAsync()
    {
        $count = count($this->getRecipients());
        if (!is_null($this->async) && $count <= self::ASYNC_LIMIT) {
            return $this->async;
        }

        return ($count >= self::ASYNC_LIMIT); // Defaults to off unless there are more than 10 recipients
    }

    /**
     * Set the template slug for the message
     * Determines whether we will ulitmately call sendMessage() or sentTemplate() on the API
     * @param  string|NULL $slug
     * @return self
     */
    public function setTemplateSlug($slug = NULL)
    {
        if (empty($slug)) {
            $slug = NULL;
        } else {
            $slug = (string) $slug;
        }
        $this->templateSlug = $slug;

        return $this;
    }

    /**
     * Return current value of template slug
     * @return string
     */
    public function getTemplateSlug()
    {
        return $this->templateSlug;
    }

    /**
     * Whether we'll be using a template or not for this message
     * @return bool
     */
    public function usesTemplate()
    {
        return !is_null($this->templateSlug);
    }

    /**
     * Set the from address
     * @param  string $email
     * @param  string $name
     * @return self
     */
    public function setFrom($email, $name = NULL)
    {
        return $this->setFromEmail($email)->setFromName($name);
    }

    /**
     * Clear the from address
     * @return self
     */
    public function clearFrom()
    {
        return $this->setFromName(NULL)->setFromEmail(NULL);
    }

    /**
     * Set From Name
     * @param  string|NULL $name
     * @return self
     */
    public function setFromName($name = NULL)
    {
        if (empty($name)) {
            unset($this->config['from_name']);
        } else {
            $this->config['from_name'] = $name;
        }

        return $this;
    }

    /**
     * Set From Email
     * @param  string|NULL $email
     * @return self
     */
    public function setFromEmail($email = NULL)
    {
        if (empty($email)) {
            unset($this->config['from_email']);
        } else {
            $this->config['from_email'] = $email;
        }

        return $this;
    }

    /**
     * Return from name
     * @return string|NULL
     */
    public function getFromName()
    {
        if (!isset($this->config['from_name'])) {
            return NULL;
        }

        return $this->config['from_name'];
    }

    /**
     * Return from email
     * @return string|NULL
     */
    public function getFromEmail()
    {
        if (!isset($this->config['from_email'])) {
            return NULL;
        }

        return $this->config['from_email'];
    }

    /**
     * Clear all to, cc and bcc recipients
     */
    public function clearRecipients()
    {
        unset($this->config['to']);

        return $this;
    }

    /**
     * Add a recipient to the message
     * @param  string                       $email
     * @param  string                       $name
     * @param  string                       $type  One of 'to', 'cc', 'bcc'
     * @return self
     * @throws Exception\ExceptionInterface
     */
    public function addRecipient($email, $name = NULL, $type = 'to')
    {
        $type = trim(strtolower($type));

        if (!in_array($type, self::$validRecipientTypes)) {
            throw new Exception\InvalidArgumentException("{$type} is not a valid recipient type");
        }

        if (!is_string($email)) {
            throw new Exception\InvalidArgumentException("Email address should be a string. Received ".gettype($email));
        }

        $email = trim(strtolower($email));
        if (empty($email)) {
            throw new Exception\InvalidArgumentException("Email address must be a non-empty string");
        }

        $spec = array(
            'email' => $email,
            'type' => $type,
        );
        if (!empty($name)) {
            $spec['name'] = $name;
        }

        if (!isset($this->config['to'])) {
            $this->config['to'] = array();
        }

        foreach ($this->config['to'] as $key => $recip) {
            if ($recip['email'] === $email) {
                $this->config['to'][$key] = $spec;
                reset($this->config['to']);

                return $this;
            }
        }
        $this->config['to'][] = $spec;

        return $this;
    }

    /**
     * Return the array containing recipient config
     * @return array
     */
    public function getRecipients()
    {
        if (!isset($this->config['to'])) {
            return array();
        }

        return $this->config['to'];
    }

    /**
     * Remove Recipient
     * @param  string $email
     * @return self
     */
    public function removeRecipient($email)
    {
        if (!isset($this->config['to'])) {
            return $this;
        }
        $email = trim(strtolower($email));
        foreach ($this->config['to'] as $key => $spec) {
            if ($spec['email'] === $email) {
                unset($this->config['to'][$key]);
            }
        }

        return $this;
    }

    /**
     * Set Subject
     * @param  string $subject
     * @return self
     */
    public function setSubject($subject)
    {
        $this->config['subject'] = (string) $subject;

        return $this;
    }

    /**
     * Get Subject
     * @return string|NULL
     */
    public function getSubject()
    {
        if (isset($this->config['subject'])) {
            return $this->config['subject'];
        }

        return NULL;
    }

    /**
     * Set the Auto-Text Flag
     * @param  bool $flag
     * @return self
     */
    public function setAutoText($flag)
    {
        return $this->setBoolParam('auto_text', $flag);
    }

    /**
     * Whether the auto_text flag is set or not
     * @return bool
     */
    public function isAutoTextExplicitySet()
    {
        return is_bool($this->getBoolParam('auto_text'));
    }

    /**
     * Return the value of the auto_text flag
     * @return bool|NULL
     */
    public function getAutoText()
    {
        return $this->getBoolParam('auto_text');
    }

    /**
     * @param  string                             $name
     * @param  bool|NULL                          $value
     * @return self
     * @throws Exception\InvalidArgumentException
     */
    public function setBoolParam($name, $value)
    {
        if (!in_array($name, $this->booleanValues)) {
            throw new Exception\InvalidArgumentException("Unknown property name {$name}");
        }
        if (!is_bool($value) && empty($value)) {
            unset($this->config[$name]);
        }
        $flag = (bool) $value;
        $this->config[$name] = $value;

        return $this;
    }

    /**
     * @param  string                             $name
     * @return bool|NULL
     * @throws Exception\InvalidArgumentException
     */
    public function getBoolParam($name)
    {
        if (!in_array($name, $this->booleanValues)) {
            throw new Exception\InvalidArgumentException("Unknown property name {$name}");
        }
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }

        return NULL;
    }

    /**
     * Set the Auto-HTML Flag
     * @param  bool $flag
     * @return self
     */
    public function setAutoHtml($flag = true)
    {
        return $this->setBoolParam('auto_html', $flag);
    }

    /**
     * Whether the auto_html flag is set or not
     * @return bool
     */
    public function isAutoHtmlExplicitySet()
    {
        return is_bool($this->getBoolParam('auto_html'));
    }

    /**
     * Return the value of the auto_html flag
     * @return bool|NULL
     */
    public function getAutoHtml()
    {
        return $this->getBoolParam('auto_html');
    }

    /**
     * Set Mandrill API Client
     * @param  MandrillClient $client
     * @return self
     */
    public function setMandrillClient(MandrillClient $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Return Mandrill Client
     * @return MandrillClient|NULL
     */
    public function getMandrillClient()
    {
        return $this->client;
    }

    /**
     * Set Message config according to values expected by te API
     * @param  array $config
     * @return self
     */
    public function setMessageConfig(array $config = array())
    {
        $this->config = $config;

        return $this;
    }

    public function setConfig(array $config = array())
    {
        if (isset($config['template_name'])) {
            $this->setTemplateSlug($config['template_name']);
        }
        if (isset($config['message'])) {
            $this->setMessageConfig($config['message']);
        }
        if (isset($config['template_content'])) {
            // No method for this yet
        }
        if (isset($config['async'])) {
            $this->setAsync($config['async']);
        }

        return $this;
    }

    /**
     * Return current message config
     * @return array
     */
    public function getMessageConfig()
    {
        return $this->config;
    }

}
