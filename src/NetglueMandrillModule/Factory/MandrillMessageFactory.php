<?php

namespace NetglueMandrillModule\Factory;

use NetglueMandrillModule\Exception;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use NetglueMandrill\Client\MandrillClient;
use NetglueMandrillModule\Message\MandrillMessage;

class MandrillMessageFactory implements FactoryInterface
{

    /**
     * The Mandrill API Client
     * @var MandrillClient
     */
    protected $client;

    /**
     * Specification arrays for messages
     * @var array
     */
    protected $messageSpecs = array();

    /**
     * Return a Mandrill Message Factory Instance
     * @return self
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $config = isset($config['netglue_mandrill']) ? $config['netglue_mandrill'] : array();

        /**
         * Store Client Instance
         */
        $client = $serviceLocator->get('NetglueMandrill\Client\MandrillClient');
        $this->setMandrillClient($client);

        /**
         * Store Message Spec
         */
        $spec = isset($config['message_spec']) ? $config['message_spec'] : array();
        $this->setMessageSpecs($spec);

        return $this;
    }

    /**
     * Create a new Message instance injected with an API client
     *
     * Optionally provide a spec name to provide configured defaults for the named message
     * @param  string                     $name
     * @return MandrillMessage
     * @throws Exception\RuntimeException if the spec identified by $name has not been set
     */
    public function createMessage($name = NULL)
    {
        $message = new MandrillMessage;
        $message->setMandrillClient($this->getMandrillClient());

        if (NULL !== $name) {
            $spec = $this->getMessageSpecByName($name);
            if (!$spec) {
                throw new Exception\RuntimeException("Unknown message spec: {$name}");
            }
            $message->setConfig($spec);
        }

        return $message;
    }

    /**
     * Return the message spec for the given name
     * @param  string     $name
     * @return array|NULL
     */
    public function getMessageSpecByName($name)
    {
        $name = (string) $name;
        if (isset($this->messageSpecs[$name])) {
            return $this->messageSpecs[$name];
        }

        return NULL;
    }

    /**
     * Set the available message specs to those provided
     * @param  array $specs
     * @return self
     */
    public function setMessageSpecs($specs)
    {
        if (!is_array($specs)) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array for the message specs',
                __METHOD__
            ));
        }
        $this->messageSpecs = array();
        foreach ($specs as $name => $spec) {
            $this->addMessageSpec($name, $spec);
        }

        return $this;
    }

    /**
     * Return entire message spec array
     * @return array
     */
    public function getMessageSpecs()
    {
        return $this->messageSpecs;
    }

    /**
     * Add a message sepcification
     * @param string $name
     * @param array  $spec
     */
    public function addMessageSpec($name, $spec)
    {
        $name = (string) $name;
        if (!is_array($spec)) {
            // For now we require an array
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array for the message spec',
                __METHOD__
            ));
        }
        $this->messageSpecs[$name] = $spec;

        return $this;
    }

    /**
     * Set Mandrill API Client
     * @param  MandrillClient $client
     * @return self
     */
    public function setMandrillClient(MandrillClient $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Return Mandrill Client
     * @return MandrillClient|NULL
     */
    public function getMandrillClient()
    {
        return $this->client;
    }

}
