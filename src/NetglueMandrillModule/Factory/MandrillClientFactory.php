<?php

namespace NetglueMandrillModule\Factory;

use NetglueMandrillModule\Exception;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetglueMandrill\Client\MandrillClient;

class MandrillClientFactory implements FactoryInterface
{
    /**
     * Return a Mandrill API Client
     * @return MandrillClient
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        if (!isset($config['netglue_mandrill'])) {
            throw new Exception\RuntimeException('No configuration was found for the Netglue Mandrill Module');
        }

        return new MandrillClient($config['netglue_mandrill']['api_key']);
  }

}
