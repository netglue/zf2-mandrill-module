<?php

namespace NetglueMandrillModule\Validator;

use Zend\Validator\AbstractValidator;

class MandrillTag extends AbstractValidator
{
    /**
     * Error: Not String
     */
    const INVALID = 'notString';

    /**
     * Error: Leading Underscore Reserved
     */
    const LEADING_UNDERSCORE = 'leadingUnderscore';

    /**
     * Error: Too Long
     */
    const TOO_LONG = 'tooLong';

    /**
     * Max String Length
     */
    const MAX_LENGTH = 50;

    /**
     * Error Message Templates
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID => "Tags should be alpha numeric strings",
        self::LEADING_UNDERSCORE => "Tags cannot begin with an underscore. These tags are reserved for internal Mandrill usage",
        self::TOO_LONG => "Tags have a maximum of 50 characters",
    );

    /**
     * Validate
     * @return bool
     * @param  string $str
     */
    public function isValid($str)
    {
        if (!is_string($str)) {
            $this->error(self::INVALID);

            return false;
        }

        $this->setValue($str);

        if (preg_match('/^_/', $str)) {
            $this->error(self::LEADING_UNDERSCORE);

            return false;
        }

        if (strlen($str) > self::MAX_LENGTH) {
            $this->error(self::TOO_LONG);

            return false;
        }

        return true;
    }

}
